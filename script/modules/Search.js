import {Select, Input} from "./components.js";
import Form from "./form.js";
import {cardsList} from "./autorisation.js";
import CardsList from "./CardsList.js";

export default class Search {
    constructor(parentSelector, cards) {
        this.createFields();
        this.form = new Form(...this.fields);
        this.form.domNode.classList.add('search-form');
        this.parent = document.querySelector(`#${parentSelector}`);
        this.cards = cards;
    }

    createFields() {
        const searchInput = new Input({
            type: 'text',
            name: 'description',
            className: 'form-control, search-input',
            placeholder: 'Search'
        });
        const optionStatus = new Select({
            options: ['All', 'Opened', 'Done'],
            onChange: value => value,
            attr: {
                name: 'status',
                id: 'select',
                className: 'form-select, search-status'
            },
            labelText: 'Choose status: '
        });
        const optionPriority = new Select({
            options: ['All', 'High', 'Normal', 'Urgent'],
            onChange: value => value,
            attr: {
                name: 'priority',
                id: 'select',
                className: 'form-select, search-priority'
            },
            labelText: 'Choose priority: '
        });
        const searchBtn = new Input({
            type: 'submit',
            name: 'search',
            value: 'search',
            className: 'search-button'
        });

        this.fields = [searchInput, optionStatus, optionPriority, searchBtn];

        searchBtn.domNode.addEventListener('click', e => {
            e.preventDefault();

            const searchInput = document.querySelector('.search-input').value;
            const optionPriority = document.querySelector('.search-priority').value;
            const optionStatus = document.querySelector('.search-status').value;

            let sortDate = this.cards.filter(item => new Date(item.dateOfVisit) < new Date());
            console.log(sortDate);


            let sortedArr = this.cards.filter(item => {
                let {description, priority} = item;
                return (optionPriority === priority || optionPriority === 'All')
                    && (description.toLowerCase().includes(searchInput) || !searchInput)
                    // && (optionStatus ===  || optionStatus === 'All')
            });

            cardsList.rerender(sortedArr);
        });
    }

    render() {
        return this.parent.append(this.form.render());
    }
}

