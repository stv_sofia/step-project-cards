import {Input} from "./components.js";
import Form from "./form.js";
import API from "./request.js";
import {BtnCreateVisit, LogOut} from "./headerBtns.js";
import {list_areas} from "./dragNdrop.js";


import { CreateVisit } from "./Visit.js";
import CardsList from "./CardsList.js";
import Search from "./Search.js";
export let cardsList;

const logInElements = {
    emailInput : {
        type: 'email',
        id: 'email',
        name: 'email',
        required: true,
        className: 'form-control',
        placeholder: 'Enter email',
        labelText: 'Email'
    },
    pswInput : {
        type: 'password',
        id: 'password',
        name: 'password',
        required: true,
        className: 'form-control',
        placeholder: 'Enter password',
        labelText: 'Password'
    },
    submitInput : {
        type: 'submit',
        id: 'submitLogin',
        name: 'submitLogin',
        value: 'submit'
    }
}

const email = new Input(logInElements.emailInput),
    psw = new Input(logInElements.pswInput),
    btnSubmitLogIn = new Input(logInElements.submitInput);

export default class Authorisation extends Form{
    constructor(btnLogin, modal) {
        super(email, psw, btnSubmitLogIn);

        this.btnSubmit = btnSubmitLogIn;
        this.btnLogin = btnLogin;
        this.modal = modal;

        this.title = 'Log In';

        this.dataFromForm = {};

        this.token = '';

        this.modal.domNode.addEventListener('click', e => {
            if(e.target.dataset.close) {
                this.domNode.reset();  // console.log( 'close and reset form-data');
            }
        })

        this.btnSubmit.domNode.addEventListener('click', e => {
            this.handleClickA(e);
        });
    }

    handleClickA(e){
        e.preventDefault();

        this.dataFromForm = this.getValue();

            this.getToken(this.dataFromForm)
                .then(r => {
                    console.log(this.token);
                    if(this.token !== 'Incorrect username or password') {
                        this.getCards()  // + проверка на наличие карточек стартует из this.getCards()

                        //показываем зону поиска для TETIANA's search-form
                        document.querySelector('.search-box').style.display = 'block';

                        // показываем зону для вывода списка драг-карточек-визита (в 3-юю колонку)
                        // если карт нет - в средней колонке отобразится >>>
                        // 'no cards have been created yet' из this.checkCards(cards);
                        list_areas.forEach(el => { el.style.display = 'flex'; });

                        // показываем кнопки на Выход и Создание Визита >>>>
                        this.btnCreateCard = new BtnCreateVisit('#headerBtnsContainer');
                        new LogOut('#headerBtnsContainer');

                        // удаляем все содержимое в модалке и закрываем/удаляем само модальное окно
                        this.close();



                        // OLENA >> событие на кнопку btnCreateCard - не трогаю - вдруг поламаю
                        this.btnCreateCard.domNode.addEventListener('click', () => {
                            this.visit = new CreateVisit();
                            this.visit.render();
                        });

                    } else {
                        alert('Wrong EMAIL or PASSWORD');
                        this.domNode.reset();
                        this.dataFromForm = {};
                    }
                })
    }

    async getToken(data) {
        console.log(data);
        return this.token = await API.authorisation({
                email: `${data.email}`,
                password: `${data.password}`
            }).then(r => r.text());
    }

    async getCards(){
        const cards = await API.getCards();  // console.log(cards);
        this.checkCards(cards);

        // TETIANA cod - не трогаю - вдруг поламаю
        let search = new Search('search', cards);
        console.log(search);
        search.render();

        // OLENA cod - не трогаю - вдруг поламаю
        cardsList = new CardsList(cards);
        cardsList.render(document.getElementById('listArea3'));
    }

    checkCards(cards) {
        this.elCheck = document.createElement('span');
        this.parentElCheck = document.getElementById('listArea2');
        this.elCheck.innerText = 'no cards have been created yet'
        this.elCheck.style.textAlign = 'center';
        this.elCheck.style.color = '#e4a565';

        (cards.length === 0) ? this.parentElCheck.append(this.elCheck) : this.elCheck.remove();
    }

    close() {
        this.modal.closeModal();
        this.domNode.reset();
        this.domNode.remove();
        this.modal.removeModal();
        this.btnLogin.domNode.remove();
    }

    render() {
        this.modal.render(this.title, super.render());
    }
}

