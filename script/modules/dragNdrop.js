export const list_areas = document.querySelectorAll('.list-area');
export const list_items = document.querySelectorAll('.list-item_draggable');

export function  setClassForDrag(el) {
    el.classList.add('list-item_draggable');
    el.setAttribute('draggable', 'true');
    return el;
}

export function dragAndDrop () {
    const list_areas = document.querySelectorAll('.list-area');
    const list_items = document.querySelectorAll('.list-item_draggable');

    // list_areas.forEach(el => {
    //     el.style.display = 'flex';
    // });

    let draggedItem = null;
    list_items.forEach((item, index) => {

        item.addEventListener('dragstart', function () {
            draggedItem = item;
            setTimeout(function () {
                item.style.display = 'none';
            }, 0)
        });

        item.addEventListener('dragend', function () {
            setTimeout(function () {
                draggedItem.style.display = 'block';
                draggedItem = null;
            }, 0);
        });

        list_areas.forEach((list, index) => {
            list.addEventListener('dragover', function (e) {
                e.preventDefault();
            });

            list.addEventListener('dragenter', function (e) {
                e.preventDefault();
                this.style.backgroundColor = 'rgba(0, 0, 0, 0.2)';
            });

            list.addEventListener('dragleave', function (e) {
                this.style.backgroundColor = 'rgba(0, 0, 0, 0.1)';
            });

            list.addEventListener('drop', function (e) {
                this.prepend(draggedItem);
                this.style.backgroundColor = 'rgba(0, 0, 0, 0.1)';
            });
        });
    });

}


export default {
    setClassForDrag,
    dragAndDrop,
    list_areas,
    list_items,

}