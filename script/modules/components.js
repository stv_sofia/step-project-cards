// COMPONENTS____________

// _______BUTTON_________
export class Button {
    constructor(name, type, textContent) {
        this.domNode = document.createElement('button');
        this.domNode.name = name;
        this.domNode.type = (type) ? type : '';
        this.domNode.textContent = textContent;
    }
    render(parent){
        parent.append(this.domNode);
    }
}

// _______BUTTON_________
export class ButtonAgain {
    constructor(id, type, textContent) {
        this.domNode = document.createElement('button');
        this.domNode.id = id;
        this.domNode.type = (type) ? type : '';
        this.domNode.classList.add('my-button');

        this.span = document.createElement('span');
        this.span.textContent = textContent;

        this.domNode.append(this.span);
    }
    render(){
        return this.domNode;
    }
}

// _______INPUT__________
export class Input {
    constructor({labelText  = false, ...attr}) {
        this.domNode = document.createElement('input');
        this.attr = attr;
        for(let [key, value] of Object.entries(this.attr)) {
            this.domNode[key] = value;
        }
        this.domNode.classList.add('form-elems');
        this.labelText = labelText;
    }

    render(){
        if(this.labelText){
            this.label = document.createElement('label');
            this.label.textContent = this.labelText;
            this.label.append(this.domNode);
            return this.label;
        }
        return this.domNode;
    }
}



//-----------------------
// _______SELECT_________
export class Select {
    constructor({options, onChange, attr, labelText, selected}) {
        if(selected) {
            this.selected = selected;
        }
        this.options = options;
        this.onChange = onChange;
        this.attr = attr;
        this.labelText = labelText;
        
    }

    render(){
        this.domNode = document.createElement('select');
        for(let [key, value] of Object.entries(this.attr)) {
            this.domNode[key] = value;
        }

        this.domNode.addEventListener('change', () => this.onChange(this.domNode.value));

        this.label = document.createElement('label');
        this.label.setAttribute("for", this.attr.id);
        this.label.classList.add('my-input');
        this.label.textContent = this.labelText;
        this.label.append(this.domNode);
        
        this.options.forEach( item => {
            let options = document.createElement('option');
            options.textContent = item;
            this.domNode.append(options);
        })
        if(this.selected) {
            
            this.domNode.value = this.selected;
        }
        return this.label;
    }
}


// _______TEXTAREA_______
export class Textarea {
    constructor({labelText  = false, ...attr}) {
       this.attr = attr;
       if(labelText) {
           this.labelText = labelText;
       }
    }

    render() {
        this.domNode = document.createElement('textarea');
        
            for(let [key, value] of Object.entries(this.attr)) {
                this.domNode[key] = value;
            }
            if(this.labelText){
                const label = document.createElement('label');
                label.classList.add('my-input');
                label.textContent = this.labelText;
                label.append(this.domNode);
                return label;
            }
            return this.domNode;
    }
}

