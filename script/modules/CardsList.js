import {Button} from "./components.js";
import Form from "./form.js";
import {Input} from "./components.js";
import { CreateVisit, VisitCardiologist, VisitDentist, VisitPhysician} from "./Visit.js";
import API from "./request.js";
import { cardsList } from "./autorisation.js";
import dragNdrop from "./dragNdrop.js";


export default class CardsList {
    constructor(cards) {
        this.cards = cards;

    }

    createCard(card) {
        const container = document.createElement('div');
        container.setAttribute('data-card-id', `${card.id}`)
        container.classList.add('card');
        
        container.insertAdjacentHTML('afterbegin', `
                <div class="modal-header card-item-header header">
                    <span class="modal-title header-title" id="modalTile">Appointment</span>
                    <button type="button" class="modal-close header-close">&times;</button>
                </div>
                <div class="modal-body card-body" id="modalBody">
                    <div class="body-container">
                        <h5 class="card-body-title"></h5>
                        <h5 class="card-body-name"></h5>
                        <div class="btn-container"></div>
                        <div class="card-body__body"></div>
                    </div>
                </div>
                <div class="modal-footer" id="modalFooter"></div>
            `);

            // DROPDOWN BUTONS
            const btnGroup = new Button('btngroup', 'button', 'edit...');
            btnGroup.domNode.classList.add('btn-group-dropdown');

            const dropDown = document.createElement('ul');
            dropDown.classList.add('btn-dropdwn');

            const btnEdit = new Button('edit', 'button', 'edit');
            btnEdit.domNode.classList.add('edit');

            const btnDelete = new Button('delete', 'button', 'delete');
            btnDelete.domNode.classList.add('delete');

            // CREATE FORM FOR EDITING
            btnEdit.domNode.addEventListener('click', (e) => {
                e.preventDefault();
                // alert("Good!");
                this.createEditForm(container, card);
            })
            btnEdit.render(dropDown);
            // DELETE BUTTON
            btnDelete.domNode.addEventListener('click', async(e) => {
                e.preventDefault();
                if(confirm('Do you really want to delete this card?')) {
                    await this.deleteCard(card);
                    container.remove();
                }
            })
            btnDelete.render(dropDown);
            //DELETE BUTTON FROM CARD HEADER
            container.querySelector('.header-close').addEventListener('click', async(e) => {
                e.preventDefault();
                if(confirm('Do you really want to delete this card?')) {
                    await this.deleteCard(card);
                    container.remove();
                }
            })

            btnGroup.domNode.append(dropDown);
            btnGroup.render(container.querySelector('.btn-container'));

            btnGroup.domNode.addEventListener('click', e => {
                e.preventDefault();
                dropDown.classList.toggle('show-buttons');
            })

            // SHOW MORE BUTTON
            const btnShowMore = new Input({
                type: 'submit',
                name: 'showMore',
                value: 'show/hide more',
                className: 'btn-show-more'
            });
            btnShowMore.domNode.addEventListener('click',(e) => {
                e.preventDefault();
                container.querySelector('.card-body__body').classList.toggle('show');
            });

            container.querySelector('.btn-container').append(btnShowMore.render());

            // CREATING ELEMENTS OF CARD BODY
            for(let item in card) {
                if(item === 'title') {
                    container.querySelector('.card-body-title').textContent = card[item];
                } else if(item === 'fullName') {
                    container.querySelector('.card-body-name').textContent = card[item];
                } else {
                    let domNodeItem = document.createElement('p');
                    let itemTitle = document.createElement('span');
                    let itemText = document.createElement('span');
                    domNodeItem.classList.add('body-card', item);
                    itemTitle.className = 'body-card-title';
                    itemText.className = 'body-card-text';
                    //CHANGING STRING FROM KEY(OBJECT)
                    let changedItem = this.changeFormat(item);
                    //END OF CHANGING STRING
                    itemTitle.textContent = `${changedItem}: `;
                    itemText.textContent = card[item]
                    domNodeItem.append(itemTitle, itemText);

                    container.querySelector('.card-body__body').append(domNodeItem);
                }
            }
            dragNdrop.setClassForDrag(container);
            this.parent.append(container);
        
        return container;
    }

    render(parent) {
        this.parent = parent;
        if(this.cards.length === 0) {
            const info = document.createElement('p');
            info.textContent = 'No items have been added';
            document.getElementById('listArea3').append(info);
            document.getElementById('listArea2').append(info);
            document.getElementById('listArea1').append(info);
        }
        
        this.cards.forEach(card => {
            this.createCard(card);
        })
        dragNdrop.dragAndDrop();

        // return container;

    }

    changeFormat(str) {
        function toLowerHyphen(match) {
            return " " + match.toLowerCase();
        }
        let changed =  str.replace(/[A-Z]/g, toLowerHyphen);
        return changed = changed.charAt(0).toUpperCase() + changed.slice(1);
    }

    async deleteCard(card) {
        await API.deleteCard(card);
        this.cards = this.cards.filter(item => item.id !== card.id);;
    }

    rerender(cards) {
        this.cards = cards;
        this.parent.innerHTML = '';
        this.render(this.parent);
    }

    updateCard(card) {
       const container = document.querySelector(`[data-card-id = "${card.id}"]`);
       container.querySelector('.card-body-name').textContent = `${card.fullName}`;
       container.querySelector('.age .body-card-text').textContent = `${card.age}`;
       container.querySelector('.priority .body-card-text').textContent = card.priority;
       container.querySelector('.purposeOfVisit .body-card-text').textContent = card.purposeOfVisit;
       container.querySelector('.description .body-card-text').textContent = card.description;
       if(container.querySelector('.lastVisit')) {
            container.querySelector('.lastVisit .body-card-text').textContent = card.lastVisit;
       }
       if(container.querySelector('.bloodPressure')) {
            container.querySelector('.bloodPressure .body-card-text').textContent = card.bloodPressure;
       }
       if(container.querySelector('.bodyMassIndex')) {
            container.querySelector('.bodyMassIndex .body-card-text').textContent = card.bodyMassIndex;
       }
       if(container.querySelector('.deseases')) {
            container.querySelector('.deseases .body-card-text').textContent = card.deseases;
       }
    }

    createEditForm(container, card) {
        const cardTitle = container.querySelector('.card-body-title');
        container.querySelector('.body-container').style.display = 'none';

        this.title = container.querySelector('#modalTile');

        switch(cardTitle.textContent) {
            case "Dentist":
                this.title.textContent = 'Edit Dentist visit';
                this.visit = new VisitDentist({ card });
                // console.log(card);
                this.inputs = this.visit.inputs;

                break;
            case "Physician":
                this.title.textContent = 'Edit Physician visit';
                this.visit = new VisitPhysician({ card });
                // console.log(card);
                this.inputs = this.visit.inputs;
                break;
            case "Cardiologist":
                this.title.textContent = 'Edit Cardiologist visit';
                this.visit = new VisitCardiologist({ card });
                // console.log(card);
                this.inputs = this.visit.inputs;
                break;
        }
        container.querySelector('#modalBody').append(this.visit.render());
    }

}