import {ButtonAgain} from "./components.js";
import Modal from "./modal.js";
import Authorisation from "./autorisation.js";
// import ALL from "./dragNdrop.js";

export class Login {
    constructor(parentSelector, clicked) {
        this.btnLogIn = new ButtonAgain('logIn', '', 'Log In');
        this.parent = document.querySelector(`${parentSelector}`);
        this.parent.append(this.btnLogIn.render());

        this.clicked = clicked;
        this.btnLogIn.domNode.addEventListener('click', e => this.handleClick(e));

        return this.btnLogIn;
    }

    handleClick(e) {
        if(!this.clicked) {
            this.modal = new Modal(this.btnLogIn);
            this.authorisation = new Authorisation(this.btnLogIn, this.modal).render();
            this.modal.openModal();
            this.clicked = true;
        } else {
            this.modal.openModal();
        }
    }
}

export class BtnCreateVisit {
    constructor(parentSelector) {
        this.btnCreateVisit = new ButtonAgain('btnCreateVisit', '', 'Create Visit');
        this.parent = document.querySelector(`${parentSelector}`);
        this.parent.append(this.btnCreateVisit.render());

        // this.btnCreateVisit.domNode.addEventListener('click', e => this.handleClick(e));

        return this.btnCreateVisit;
    }

    // handleClick(e) { e.preventDefault(); }
}

export class LogOut {
    constructor(parentSelector) {
        this.btnLogOut = new ButtonAgain('logOut', '', 'Log Out');
        this.parent = document.querySelector(`${parentSelector}`);
        this.parent.append(this.btnLogOut.render());

        this.btnLogOut.domNode.addEventListener('click', e => this.handleClick(e));
    }

    handleClick(e) {
        // ALL.list_areas.forEach(el => {
        //     el.style.display = 'none';
        // });
        // document.querySelector('.search-box').style.display = 'none';
        //
        // this.btnLogOut.domNode.remove();
        // document.getElementById('btnCreateVisit').remove();

        window.location.reload();
    }
}

export default {
    Login,
    BtnCreateVisit,
    LogOut,
}