export default class Form {
    constructor(...elements) {
        this.domNode = document.createElement('form');
        this.domNode.id = 'my-form';
        this.domNode.classList.add('my-form');
        this.domNode.setAttribute('onsubmit', 'return false');
        this.elements = elements;
    }

    getValue() {
        let valueFromElem = {};
        this.elements.map((el)=> {
            if (el.domNode.name !== 'submitLogin' && el.domNode.name !== 'submit') {

                if(el.domNode.value !== ''){
                    valueFromElem[el.domNode.name] = el.domNode.value;
                } else {
                    throw new Error(`fill the ${el.domNode.name} field`);
                }
            }
        });
        return valueFromElem;
    }

    getDomNodeForm() {
        return this.domNode;
    }

    render() {
        this.elements.forEach( el => {
            this.domNode.append(el.render())
        })
        return this.domNode;
    }

}