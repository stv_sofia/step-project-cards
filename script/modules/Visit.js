import { Input, Select,Textarea} from "./components.js";
import Form from "./form.js";
import Modal from './modal.js';
import API from "./request.js";
import CardsList from "./CardsList.js";
import {cardsList} from "./autorisation.js";


class CreateVisit {
    constructor() {
        this.modalContent =  new Select({
            options: ['', 'Dentist', 'Physician', 'Cardiologist'],
            onChange: value => this.switchToVisitForm(value),
            attr: {
                name: 'select',
                id: 'select',
                required: true,
                className: 'form-select'
            },
            labelText: 'Choose your doctor: '
        });

        this.titleText = 'Select visit type:';

        this.modal = new Modal();
    }

    switchToVisitForm(visitType) {
        const onClose = () => this.close();

        switch (visitType) {
            case 'Dentist':
                this.titleText = 'Create Dentist visit:';
                this.modalContent = new VisitDentist({ onClose });
                break;
            case 'Physician':
                this.titleText = 'Create Physician visit:';
                this.modalContent = new VisitPhysician({ onClose });
                break;
            case 'Cardiologist':
                this.titleText = 'Create Cardiologist visit';
                this.modalContent = new VisitCardiologist({ onClose });
                break;
            default:
                this.titleText = 'Create visit:';
                this.modalContent = new Visit({ onClose });
        }

        this.modalContent.createInputs();
        this.modal.title.innerHTML = '';
        this.modal.body.innerHTML = '';
        this.render();
    }

    close() {
        this.modal.closeModal();
    }

    render() {
        return this.modal.render(this.titleText, this.modalContent.render());
    }
}

class Visit {
    constructor({ onClose, card }) {
        if (card) {
            this.card = card;
        }
        this.createInputs();
        this.form = new Form(...this.inputs);
        this.onClose = onClose;
    }

    createInputs() {

        this.fullName = new Input({
            type: 'text',
            name: 'fullName',
            required: true,
            className: 'form-control',
            placeholder: 'Enter your name',
            labelText: 'Full Name',
            value: this.card ? this.card.fullName : ''
        });
        this.age = new Input({
            type: 'number',
            name: 'age',
            required: true,
            className: 'form-control',
            placeholder: 'Enter your age',
            labelText: 'Age',
            value: this.card ? this.card.age : ''
        });
        this.dateOfVisit = new Input({
            type: 'date',
            name: 'dateOfVisit',
            required: true,
            labelText: 'Choose the date of visit: ',
            className: 'form-control',
            value: this.card ? this.card.dateOfVisit : '',
            disabled: this.card ? true : false
        });
        this.priority = new Select({
            options: ['', 'Normal', 'High', 'Urgent'],
            onChange: value => value,
            attr: {
                name: 'priority',
                required: true,
                className: 'form-select',
            },
            labelText: 'Choose your priority: ',
            selected: this.card ? this.card.priority : ''
        });
        this.purposeOfVisit = new Input({
            type: 'text',
            name: 'purposeOfVisit',
            required: true,
            className: 'form-control',
            placeholder: 'Enter your purpose of visit',
            labelText: 'Purpose of visit',
            value: this.card ? this.card.purposeOfVisit : ''
        });
        this.shortDescr = new Textarea({
            name: 'description',
            required: true,
            className: 'form-control',
            placeholder: 'Short description of visit',
            labelText: 'Short description of visit',
            value: this.card ? this.card.description : ''
        });
        this.submitBtn = new Input({
            type: 'submit',
            name: 'submit',
            className: 'form-btn',
            value: this.card ? 'save' : 'create'
        });

        this.inputs = [this.fullName, this.age, this.dateOfVisit, this.priority, this.purposeOfVisit, this.shortDescr];


        this.submitBtn.domNode.addEventListener('click', e => {
            e.preventDefault();
            if(this.card) {
                API.editCard({...this.getData(), id: this.card.id}).then(res => {
                    document.querySelector(`[data-card-id="${this.card.id}"] .body-container`).style.display = 'block';
                    const container = document.querySelector(`[data-card-id="${this.card.id}"] #modalBody`);
                    container.querySelector('#my-form').remove();
                    const infoToEdit = res.content;
                    Object.assign(this.card, infoToEdit);
                    cardsList.updateCard(infoToEdit);
                  
                });
            } else {
                API.addCard(this.getData())

                    .then(res => {
                        cardsList.cards = [...cardsList.cards, res];
                        cardsList.createCard(res)});
            }
            if(this.onClose) this.onClose();
        })

    }

    render() {
        return this.form.render();
    }

    getData() {
        this.dataToSubmit = {
            ...this.form.getValue(),
            title: this.title || 'Doctor'
        };

        return this.dataToSubmit;
    }

}

class VisitDentist extends Visit {

    title = "Dentist";

    createInputs() {
        super.createInputs();

        this.lastVisit = new Input({
            type: 'date',
            name: 'lastVisit',
            required: true,
            labelText: 'Last visit date: ',
            className: 'form-control',
            value: this.card ? this.card.lastVisit : ''
        });

        this.inputs = [
            ...this.inputs,
            this.lastVisit,
            this.submitBtn
        ];
    }
}

class VisitPhysician extends Visit {

    title = "Physician";

    createInputs() {
        super.createInputs();

        this.inputs = [
            ...this.inputs,
            this.submitBtn
        ];
    }
}

class VisitCardiologist extends Visit {

    title = "Cardiologist";

    createInputs() {
        super.createInputs();

        this.bloodPressure = new Input({
            type: 'number',
            name: 'bloodPressure',
            required: true,
            className: 'form-control',
            placeholder: 'Enter your blood pressure',
            labelText: 'Blood pressure',
            value: this.card ? this.card.bloodPressure : ''
        });

        this.bodyMI = new Input({
            type: 'number',
            name: 'bodyMassIndex',
            required: true,
            className: 'form-control',
            placeholder: 'Enter your BMI(Body mass index)',
            labelText: 'BMI',
            value: this.card ? this.card.bodyMassIndex : ''
        });

        this.prevDiseases = new Textarea({
            name: 'deseases',
            required: true,
            className: 'form-control',
            placeholder: 'Enter you previous deseases',
            labelText: 'Your previous deseases',
            value: this.card ? this.card.deseases : ''
        });

        this.inputs = [
            ...this.inputs,
            this.bloodPressure,
            this.bodyMI,
            this.prevDiseases,
            this.submitBtn
        ];
    }
}

export {
    CreateVisit,
    VisitDentist,
    VisitPhysician,
    VisitCardiologist
}