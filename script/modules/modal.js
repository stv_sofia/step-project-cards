// Добавляя обработчик событий на кнопку, обратите внимание ≥≥≥
// Использование class Modal>>> по замыслу - динамическое создание одного экземпляра этого класса,
// и все нужные изменения происходят внутри него, а не создается новый экземпляр, а предыдущий висит где-то там...
// При закрытии модального окна - оно не удаляется, а вне зоны видимости ждет вызова на открытие (по кнопке на которой былл создан),
// если в обработчике на кнопке (или дальше по коду) это не отследить, т е забыть про этот экземпляр - не сказать ему что делать дальше,
// то при нажатии на эту-же кнопку будет создаваться уже новый экземпляр.
// ---≥≥ если в вашем коде в модальном окне больше нет необходимости - желательео удалять этот экземпляр - метод в классе описан.
// ---≥≥ если при закрытии окна нужно очистить его содержимое или напр. данные из формы >>> на пример: autorisation.js:59

export default class Modal{
    constructor() {
        this.domNode = this.createModal();
        this.title = this.domNode.querySelector('#modalTile');
        this.body = this.domNode.querySelector('#modalBody');

        this.domNode.addEventListener('click', e => {
            this.handleClickModal(e);
        });
    }

    createModal(){
        const modal = document.createElement('div');
        modal.classList.add('new-modal');
        modal.insertAdjacentHTML('afterbegin', `
        <div class="modal-overlay" data-close="true">
            <div class="modal-window" id="modalWindow">
                <div class="modal-header">
                    <span class="modal-title" id="modalTile"></span>
                    <span class="modal-close" data-close="true">&times;</span>
                </div>
                <div class="modal-body" id="modalBody"></div>
                <div class="modal-footer" id="modalFooter"></div>
            </div>
        </div>`);
        return modal;
    }

    handleClickModal(e) {
        if(e.target.dataset.close){
            this.closeModal();
        }
    }

    openModal() {
        setTimeout(() => {
            this.domNode.classList.add('open');
        }, 100);
    }

    closeModal() {
        this.domNode.classList.remove('open');
        this.domNode.classList.add('hiding');
        setTimeout(() => {
            this.domNode.classList.remove('hiding');
        }, 1000);
    }

    removeModal() {
        setTimeout(() => {
            this.domNode.remove();
        }, 1000);
    }

    render(title, form){
        this.title.append(title);
        this.body.append(form);
        document.body.append(this.domNode);
        this.openModal();
    }
}