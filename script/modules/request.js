const LINK = {
    cards: 'https://ajax.test-danit.com/api/v2/cards',
    login: 'https://ajax.test-danit.com/api/v2/cards/login'
};

const getHeadersTemplate = () => ({
    'Content-Type': 'application/json',
    'Authorization': localStorage.getItem('token')
        ? `Bearer ${localStorage.getItem('token')}`
        : undefined,
});

const authorisation = (user) => {
    return fetch(LINK.login, {
        method: 'POST',
        headers: getHeadersTemplate(),
        body: JSON.stringify(user)
    })
};

const getCards = () => {
    return fetch(LINK.cards, {
        headers: getHeadersTemplate(),
    }).then(r => r.json());
};

const addCard = (cardObj) => {
    return fetch(LINK.cards, {
        method: "POST",
        body: JSON.stringify(cardObj),
        headers: getHeadersTemplate()
    }).then(r => r.json());
};

const editCard = (cardObj) => {
    return fetch(`https://ajax.test-danit.com/api/cards/${cardObj.id}`, {
        method: "PUT",
        body: JSON.stringify(cardObj),
        headers: getHeadersTemplate(),
    }).then(r => r.json());
};

const deleteCard = (cardObj) => {
    return fetch(`https://ajax.test-danit.com/api/cards/${cardObj.id}`, {
        method: 'DELETE',
        // body: JSON.stringify(cardObj),
        headers: getHeadersTemplate(),
    });
}


export default {
    authorisation,
    getCards,
    addCard,
    editCard,
    deleteCard
}



